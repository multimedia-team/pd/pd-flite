pd-flite (0.3.3-5) unstable; urgency=medium

  * Add patch to fix FTBFS with gcc-14 (Closes: #1075371)
  * Switch to dh-sequence-puredata
    + Add Provides stanza
  * Bump standards version to 4.7.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 04 Aug 2024 22:22:44 +0200

pd-flite (0.3.3-4) unstable; urgency=medium

  * Re-build for both single and double-precision Pd
  * Bump standards version to 4.6.2

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 05 Jul 2023 14:08:40 +0200

pd-flite (0.3.3-3) unstable; urgency=medium

  * Explicitly set buildsystem to 'pd_lib_builder'
  * Modernize 'licensecheck' target
    + Ensure that 'licensecheck' is run with the C.UTF-8 locale
    + Exclude debian/ from 'licensecheck'
    + Re-generate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 07 Dec 2022 10:41:41 +0100

pd-flite (0.3.3-2) unstable; urgency=medium

  * Add puredata:Recommends and puredata:Suggests
  * Modernize 'licensecheck' target
    + Re-generate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 05 Dec 2022 11:26:22 +0100

pd-flite (0.3.3-1) unstable; urgency=medium

  * New upstream version 0.3.3
    + Drop obsolete patches
  * Switch to dh-sequence-pd-lib-builder
  * Fix path to docs
  * Update d/copyright
    + Update 'licensecheck' target
    + Regenerate d/copyright_hints
    + Update d/copyright
  * Add Repository to upstream/metadata
  * Upstream version mangling (for 'test'-releases)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 16 Nov 2022 23:57:32 +0100

pd-flite (0.02.3-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Add salsa-ci configuration
  * Remove obsolete file d/source/local-options
  * Declare that building this package does not require 'root' powers.
  * Apply "warp-and-sort -ast"
  * Update d/watch
  * Bump dh-compat to 13
  * Bump standards version to 4.6.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 16 Aug 2022 13:53:31 +0200

pd-flite (0.02.3-4) unstable; urgency=medium

  * Enabled hardening
  * Updated Vcs-* stanzas to salsa.d.o
  * Updated maintainer address
  * Simplified & unified d/rules
    * Bumped dh compat to 11
  * Updated d/copyright(_hints)
  * Dropped B-D on dh-autoreconf (no longer needed)
  * Removed trailing whitespace in debian/*
  * Updated homepage field to github
  * Use github in d/watch
  * Enabled licensecheck target
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 01 Feb 2018 23:05:41 +0100

pd-flite (0.02.3-3) unstable; urgency=medium

  * Fixed helppatch-install-dir patch (Closes: #863658)
  * Refreshed patches (fuzz offset)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 29 May 2017 22:58:45 +0200

pd-flite (0.02.3-2) unstable; urgency=medium

  * Fixed permissions of externals (Closes: #715909)
  * Patch for 64bit compatible array access (Closes: #792718)
  * Fixed reproducible build.
    Thanks to Chris Lamb <lamby@debian.org> (Closes: #834302)
  * d/control fixup
  * Added myself to uploaders
  * Use canonical https:// uris for Vcs-* stanzas
  * Bumped debhelper compat to 9
  * Bumped standars version to 3.9.8

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 10 Nov 2016 10:36:40 +0100

pd-flite (0.02.3-1) unstable; urgency=low

  * Initial release (Closes: #643838)

 -- Roman Haefeli <reduzent@gmail.com>  Tue, 08 Nov 2011 23:13:13 +0100
